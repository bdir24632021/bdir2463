package bdir2463MV.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TasksOperationsTest {
    private TasksOperations tasksOperations;
    private Date start1ec;
    private Date start2ec;
    private Date end1ec;
    private Date end2ec;
    private Date start1bva;
    private Date start2bva;
    private Date end1bva;
    private Date end2bva;

    @BeforeEach
    void setUp() {
        Task task1 = new Task("task1", new Date(2021, Calendar.MARCH, 19), new Date(2021, Calendar.MARCH, 20),1);
        Task task2 = new Task("task2", new Date(2021, Calendar.APRIL, 01), new Date(2021, Calendar.APRIL, 02),1 );
        Task task3 = new Task("task3", new Date(2021, Calendar.JANUARY, 20), new Date(2021, Calendar.JANUARY, 21),1);
        List<Task> tasks = new ArrayList<>();
        task1.setActive(true);
        task2.setActive(true);
        task3.setActive(true);
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        tasksOperations = new TasksOperations(FXCollections.observableArrayList(tasks));
        start1ec = new Date(2021, Calendar.MARCH, 18);
        start2ec = new Date(2021, Calendar.MAY, 15);
        end1ec = new Date(2021, Calendar.JUNE, 20);
        end2ec = new Date(2021, Calendar.MAY, 10);

        start1bva = new Date(2021, Calendar.MAY, 01);
        start2bva = new Date(2021, Calendar.APRIL, 30);
        end1bva = new Date(2021, Calendar.JUNE, 01);
        end2bva = new Date(2021, Calendar.JUNE, 02);
    }

    @AfterEach
    void tearDown() {
    }

//    @DisplayName("EC Valid")
    @Test
    void test_EC_Valid() {
        int counter = 0;

        for (Task task : tasksOperations.incoming(start1ec, end1ec)) {
            counter++;
        }
        assert (counter == 2);
    }

//    @DisplayName("EC NonValid1")
    @Test
    void test_EC_Non_Valid_1() {
        int counter = 0;
        for (Task task : tasksOperations.incoming(start1ec, end2ec)) {
            counter++;
        }
        assert (counter == 0);
    }

    @DisplayName("EC NonValid2")
    @Test
    void test_EC_Non_Valid_2() {
        int counter = 0;
        for (Task task : tasksOperations.incoming(start2ec, end1ec)) {
            counter++;
        }
        assert (counter == 0);
    }

    @Disabled("EC NonValid3")
    @Test
    void test_EC_Non_Valid_3() {
        int counter = 0;
        for (Task task : tasksOperations.incoming(start2ec, end2ec)) {
            counter++;
        }
        assert (counter == 0);
    }

//    @DisplayName("BVA Valid")
    @Test
    void test_BVA_Valid() {
        int counter = 0;
        for (Task task : tasksOperations.incoming(start2bva, end2bva)) {
            counter++;
        }
        assert (counter == 0);
    }

//    @DisplayName("BVA NonValid1")
    @Test
    void test_BVA_Non_Valid_1() {
        int counter = 0;
        for (Task task : tasksOperations.incoming(start1bva, end2bva)) {
            counter++;
        }
        assert (counter == 0);
    }

    @DisplayName("BVA NonValid2")
    @Test
    void test_BVA_Non_Valid2() {
        int counter = 0;
        for (Task task : tasksOperations.incoming(start2bva, end1bva)) {
            counter++;
        }
        assert (counter == 0);
    }

    @Disabled("BVA NonValid3")
    @Test
    void test_BVA_Non_Valid_3() {
        int counter = 0;
        for (Task task : tasksOperations.incoming(start1bva, end1bva)) {
            counter++;
        }
        assert (counter == 0);
    }
}