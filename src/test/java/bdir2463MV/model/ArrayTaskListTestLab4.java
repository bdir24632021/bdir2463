package bdir2463MV.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ArrayTaskListTestLab4 {
    private ArrayTaskList arrayTaskList;
    private Task task1;
    private Task task2;
    private Task task3;

    @BeforeEach
    void setUp() {
        task1 = mock(Task.class);
        task2 = mock(Task.class);
        task3 = mock(Task.class);
        arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task1);
        arrayTaskList.add(task2);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testMockito2_Integrare() {
        Mockito.when(task2.getTitle()).thenReturn("MockTask2");
        assert(arrayTaskList.getTask(1).getTitle().equals("MockTask2"));
    }
    @Test
    void add() {
        arrayTaskList.add(task3);
        assert(arrayTaskList.size() == 3);
    }

    @Test
    void size() {
        assert(arrayTaskList.size() == 2);
    }
}