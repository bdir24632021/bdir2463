package bdir2463MV.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {

    private Task task1;
    private Task task2;

    @BeforeEach
    void setUp() {
        task1 = new Task("task1", new Date(2021, Calendar.MARCH, 20));
        task2 = new Task("task2", new Date(2021, Calendar.MARCH, 21));
        task2.setActive(false);
    }

    @AfterEach
    void tearDown() {
    }

    @DisplayName("Title test")
    @Test
    void getTitle() {
        assert(task1.getTitle().equals("task1"));
    }

    @DisplayName("Is active test")
    @Test
    void isActive() {
        assert (!task2.isActive());
    }
}