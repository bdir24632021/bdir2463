package bdir2463MV.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Calendar;
import java.util.Date;

class ArrayTaskListTest {
    private ArrayTaskList arrayTaskList1;
    private ArrayTaskList arrayTaskList2;
    private ArrayTaskList arrayTaskList3;
    private ArrayTaskList arrayTaskList4;
    private ArrayTaskList arrayTaskList5;
    private ArrayTaskList arrayTaskList6;

    Double double1;


    @BeforeEach
    void setUp() {
        arrayTaskList1 = new ArrayTaskList(); //vid
        arrayTaskList2 = new ArrayTaskList(); //vid
        arrayTaskList3 = new ArrayTaskList(); //task4
        arrayTaskList4 = new ArrayTaskList(); //task3
        arrayTaskList5 = new ArrayTaskList(); //different
        arrayTaskList6 = new ArrayTaskList(); //different, same dimension


        Task task1 = new Task("task1", new Date(2021, Calendar.MARCH, 20));
        Task task2 = new Task("task2", new Date(2021, Calendar.MARCH, 21));
        Task task3 = new Task("task3", new Date(2021, Calendar.MARCH, 22));
        Task task4 = new Task("task4", new Date(2021, Calendar.MARCH, 23));
        Task task5 = new Task("task5", new Date(2021, Calendar.MARCH, 24));
        arrayTaskList3.add(task1);
        arrayTaskList3.add(task2);
        arrayTaskList3.add(task3);
        arrayTaskList4.add(task1);
        arrayTaskList4.add(task2);
        arrayTaskList4.add(task3);
        arrayTaskList5.add(task1);
        arrayTaskList5.add(task5);
        arrayTaskList6.add(task1);
        arrayTaskList6.add(task3);
        arrayTaskList6.add(task5);


        double1 = 10.5;
    }

    @AfterEach
    void tearDown() {
    }

    @DisplayName("StatementCoverage")
    @Test
    void test_SC() {
        //testam linia 109
        assert(arrayTaskList1.equals(arrayTaskList1));

        //testam linia 111
        assert(!arrayTaskList1.equals(null));

        //testam linia 116
        assert(!arrayTaskList1.equals(arrayTaskList5));

        //testam linia 120
        assert(!arrayTaskList3.equals(arrayTaskList6));

        //testam linia 124
        assert(arrayTaskList3.equals(arrayTaskList4));
    }

    @DisplayName("MultipleConditionCoverage")
    @Test
    void test_MCC() {
        //testam conditia din if-ul de la linia 110
        //o == null || getClass() != o.getClass()
        assert(!arrayTaskList1.equals(null));
        assert(!arrayTaskList1.equals(double1));
    }

    @DisplayName("SimpleLoopCoverage")
    @Test
    void test_SLC() {
        //omiterea buclei
        assert(arrayTaskList1.equals(arrayTaskList2));

        //1 parcurgere a buclei
        assert(!arrayTaskList3.equals(arrayTaskList6));

        //n parcurgeri a buclei
        assert(arrayTaskList3.equals(arrayTaskList4));
    }

    @DisplayName("Decision/ConditionCoverage-CoversAllPaths")
    @Test
    void test_DCC() {
        //path1
        //testam conditia din if-ul de la linia 108
        //this == o
        assert(arrayTaskList1.equals(arrayTaskList1));

        //path2
        //testam conditia din if-ul de la linia 110
        //implicit s-a testat si if-ul de la linia 108
        //o == null || getClass() != o.getClass()
        assert(!arrayTaskList1.equals(null));

        //path3
        //implicit s-a testat if-ul de la 110
        //testam conditia din if-ul de la linia 115
        assert(!arrayTaskList1.equals(arrayTaskList5));

        //path4
        //implicit s-a testat if-ul de la 115
        //testam conditia din if-ul de la linia 119
        assert(!arrayTaskList3.equals(arrayTaskList6));

        //path5
        //implicit s-a testat if-ul de la 119
        //testam conditia din if-ul de la linia 119
        assert(arrayTaskList3.equals(arrayTaskList4));
    }


}