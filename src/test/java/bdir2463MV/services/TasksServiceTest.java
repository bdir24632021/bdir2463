package bdir2463MV.services;

import bdir2463MV.model.ArrayTaskList;
import bdir2463MV.model.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.shortThat;
import static org.mockito.Mockito.mock;

class TasksServiceTest {
    private TasksService tasksService;
    private ArrayTaskList arrayTaskList;
    private Task task1;

    @BeforeEach
    void setUp() {
        task1 = mock(Task.class);
        arrayTaskList = mock(ArrayTaskList.class);
        tasksService = new TasksService(arrayTaskList);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getIntervalInHours1() {
        Mockito.when(task1.getRepeatInterval()).thenReturn(1200);
        assert(tasksService.getIntervalInHours(task1).equals("00:20"));
    }

    @Test
    void getIntervalInHours2() {
        Mockito.when(task1.getRepeatInterval()).thenReturn(3660);
        assert(tasksService.getIntervalInHours(task1).equals("01:01"));
    }
}