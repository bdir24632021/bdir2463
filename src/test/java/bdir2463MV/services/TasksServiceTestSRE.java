package bdir2463MV.services;

import bdir2463MV.model.ArrayTaskList;
import bdir2463MV.model.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class TasksServiceTestSRE {
    private TasksService tasksService;
    private ArrayTaskList arrayTaskList;
    private Task task1;

    @BeforeEach
    void setUp() {
        task1 = mock(Task.class);
        arrayTaskList = mock(ArrayTaskList.class);
        tasksService = new TasksService(arrayTaskList);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void integrareSRE1() {
        Mockito.when(task1.getTitle()).thenReturn("Vasile");
        Mockito.when(arrayTaskList.getAll()).thenReturn(Arrays.asList(task1));
        assert (arrayTaskList.getAll().get(0).getTitle().equals("Vasile"));
    }

    @Test
    void integrareSRE2() {
        Mockito.when(task1.getRepeatInterval()).thenReturn(1200);
        Mockito.when(arrayTaskList.getAll()).thenReturn(Arrays.asList(task1));
        assert (arrayTaskList.getAll().get(0).getRepeatInterval() == 1200);
    }
}